#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <time.h>
#include <stdlib.h>

void stdin_clean()
{
	while(getchar() != '\n');
}

int main()
{
	int randNumber, userInput;
	int attemptsCount = 0, isWrongInput = 0, isWrongNumber = 0;
	srand((unsigned int)time(0));
	randNumber = rand() % 101;
	puts("Guess my number? (0..100)");
	do {
		isWrongInput = scanf("%d", &userInput) != 1;
		isWrongNumber = userInput != randNumber;
		if(isWrongInput) {
			puts("Unable to parse your number");
			stdin_clean();
		} else {
			if(isWrongNumber) {
				printf("Your number is %s than mine\n", userInput > randNumber ? "greater" : "smaller");
				attemptsCount++;
			} else {
				printf("You`re win!\nNumber %d was guessed in %d attempts\n", randNumber, attemptsCount);
			} 
		}
	} while (isWrongInput || isWrongNumber);
	return 0;
}