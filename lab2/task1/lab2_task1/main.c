#define _CRT_SECURE_NO_DEPRECATE

#define SQUARE(x) (x)*(x)

#include <stdio.h>
#include <time.h>

int main()
{
	float userHeight = 0;
	const float G = 9.81f;
	int secondsCounter = 0;
	float currentPath = 0;
	clock_t now;

	puts("Enter bomb drop height (meters):");
	if(scanf("%f", &userHeight) != 1 || userHeight > 10000) {
		puts("Wrong height!");
		return 1;
	}

	while(currentPath < userHeight) {
		currentPath = (float)(G * SQUARE(secondsCounter)) / 2;
		if(currentPath < userHeight) {
			printf("at %d-nth second height is %.2f meters\n", secondsCounter, userHeight - currentPath);
		} else {
			puts("Landed!");
			break;
		}		
		now = clock(); while(clock() < now + 1000);
		secondsCounter++;
	}

	return 0;
}