#define _CRT_SECURE_NO_DEPRECATE

#include <stdio.h>
#include <math.h>

/**
 * Truncates stdin out from results of previous failed operation, if any
 */
void stdin_clean()
{
	while(getchar() != '\n');
}

/**
 *  Draw triangle from asterisks with given displacement from last cursor position
 */
void draw_triangle(int height, int left, int top) 
{
	int padding, asterisksCount, iteration = 1;
	while(top > 1) {
		putchar('\n');
		top--;
	}
	while(iteration <= height) {
		asterisksCount = (iteration * 2) - 1;
		padding = left + height - iteration;
		while(padding > 0) {
			putchar('.');
			padding--;
		}
		while(asterisksCount > 0) {
			putchar('*');
			asterisksCount--;
		}
		putchar('\n');
		putchar('\r');
		iteration++;
	}
}

/**
 * calculate sum of arithmetic progression
 * s=(2*a+(n-1)*d)*(n/2)
 */
int calc_progression_sum(int a, int n, int d)
{
	return (2*a+(n-1)*d)*(n/2);
}

int calc_n_progression_member(int a, int n, int d)
{
	return a + (n - 1) * d;
}

/**
 * calculate number of required fir sections using arithmetic progression
 * (formula is gained via online service http://www.numberempire.com/equationsolver.php)
 * n = (sqrt(8*d*s+d^2-4*a*d+4*a^2)+d-2*a)/(2*d)
 */
double calc_progression_elements(int sum, int firstElement, int difference) 
{
	return (sqrt(8*difference*sum + pow(difference, 2.0f) - 4*firstElement*difference + 4*pow(firstElement, 2.0f)) + difference - 2 * firstElement) / (2*difference);
}

void promt_for_int(char * message, int * variable)
{
	int res = 0;
	puts(message); 
	while (res != 1) {
		res = scanf("%d", variable);
		if(res != 1) puts("Valid integer value is required");
		stdin_clean();
	}
	return;
}

int main()
{
	// minimal fir height
	int firHeight;
	// height of first triangle
	int initialTriangleHeight;
	// how many rows are added to the next created triangle
	int newTriangleIteration;
	// left displacement
	int left;
	// how many triangles are used to build whole fir
	int triangles;
	// width of most bottom triangle
	int bottomWidth;
	// synthetic counter of passed iterations
	int iteration = 0;

	promt_for_int("Total fir height:", &firHeight);
	promt_for_int("Initial triangle height (of first triangle):", &initialTriangleHeight);
	promt_for_int("New triangle iteration (how many rows are added to the next created triangle):", &newTriangleIteration);

	// calculate how many triangles do we need to build fir?
	triangles = ceil(calc_progression_elements(firHeight, initialTriangleHeight, newTriangleIteration));

	// calculate width of bottom triangle
	bottomWidth = calc_n_progression_member(1, (initialTriangleHeight + (triangles - 1) * newTriangleIteration), 2);

	left = 	bottomWidth / 2;

	while(iteration < triangles) {
		draw_triangle(initialTriangleHeight, left, 0);
		// increase height of next triangle
		initialTriangleHeight += newTriangleIteration;
		// need to decrease left displacement
		left -= newTriangleIteration;
		iteration++;
	}

	return 0;
}