#include <stdio.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int is_digit(char ch)
{
	return ch >= 48 && ch <= 57;
}

int is_letter(char ch)
{
	return ch >= 41 && ch <= 90 ||  ch >= 97 && ch <= 122;
}

int main()
{
	char str[256];
	char tmp;
	int replacements = 0;
	unsigned int i;
	unsigned int stringLength;
	int iterations = 0;
	int a, b, offset;

	puts("Any string including digits and alphabet letters:");
	fgets(str, 256, stdin);
	chomp(str);
	
	stringLength = strlen(str);

	for(i = 0; i < stringLength; i++) {
		if(!is_digit(str[i]) && !is_letter(str[i])) {
			puts("String contains unexpected characters");
			return 1;
		}
	}	

	offset = 'z' - '0' + 1;

	do {
		replacements = 0;
		for(i = 0; stringLength > 0 && i < stringLength - 1; i++) {

			a = str[i];
			b = str[i + 1];

			// as we need to place alphabet letters before digits we need
			// artificially increase compared 'codes' in order to guarantee
			// that any digit became 'greater' than letter
			if(is_digit(a)) a += offset;
			if(is_digit(b)) b += offset;

			if(a > b) {
				tmp = str[i + 1];
				str[i + 1] = str[i];
				str[i] = tmp;
				replacements++;
			}
			iterations++;
		}
	} while (replacements > 0);

	puts(str);
	printf("sorted in %d iterations\n", iterations);

	return 0;
}