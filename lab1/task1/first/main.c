#include <stdio.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int main()
{
	char userSex[2];
	unsigned int userWeight;
	unsigned int userHeight;
	unsigned int healthIsOk = 0;
	unsigned int isMale = 0;
	unsigned int isFemale = 0;

	puts("Lets check your health.");
	puts("Your sex please?(m/f)");
	fgets(userSex, 2, stdin);
	chomp(userSex);

	isMale = strcmp(userSex, "m") == 0;
	isFemale = strcmp(userSex, "f") == 0;

	if(!isMale && !isFemale) {
		puts("Invalid value for 'Sex'..");
		return 1;
	}

	puts("So, what is your weight?");
	if(scanf("%d", &userWeight) != 1) {
		puts("Unable to parse your age..");
		return 1;
	}

	puts("And finally, your height please?");
	if(scanf("%d", &userHeight) != 1) {
		puts("Unable to parse your height..");
		return 1;
	}
	
	healthIsOk = userWeight == (userHeight - (isMale ? 100 : 110));

	if(healthIsOk) {
		puts("You are good");
	} else {
		puts("Something is wrong with you!");
	}

	return 0;
}

