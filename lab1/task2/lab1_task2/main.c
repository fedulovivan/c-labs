#include <stdio.h>

int main()
{
	const float goldPrice = 1470.00f;
	float userWeight;

	puts("This program calculates price of gold which is equivalent to your weight");
	puts("Please enter your weight");
	if(scanf("%f", &userWeight) != 1) {
		puts("Unable to parse weight value");
		return 1;
	}

	printf("Price of %.2f grams of gold is %.2f rubles\n", userWeight*1000, userWeight*1000*goldPrice);
	return 0;
}