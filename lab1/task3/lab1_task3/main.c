#include <stdio.h>

int main()
{
	unsigned int hours;
	unsigned int minutes;
	
	printf("Your current time?(hh:mm)\n");
	if(scanf("%d:%d", &hours, &minutes) != 2) {
		printf("Invalid time format!\n");
		return 1;
	}

	if(hours > 0 && hours <= 11) {
		printf("morning!\n");
	} else if(hours > 11 && hours <= 23) {
		printf("good afternoon!\n");
	} else {
		printf("hours should be in range 0-23\n");
		return 1;
	}

	return 0;

}
