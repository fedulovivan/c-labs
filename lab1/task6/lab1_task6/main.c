
#include <stdio.h>
#include <string.h>


int main()
{
	int termCols = 80;
	char userString[64];
	int	userStringLength;
	char pattern[10];
	int padding;

	printf("Any string please:\n");
	fgets(userString, 64, stdin);

	userStringLength = strlen(userString);

	padding = (termCols + userStringLength) / 2;

	sprintf(pattern, "%%%ds", padding);
	
	printf(pattern, userString);

	return 0;
}
