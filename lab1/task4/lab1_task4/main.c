#include <stdio.h>
#include <string.h>

int main()
{
	const float PI = 3.1415926535f;
	float magnitude;
	char type;
    float result;
	int isRadians = 0;
	int isDegrees = 0;
	
	puts("Please enter an angle value. 45.00D and 45.00R formats are acceptable.");
	if(scanf("%f%c", &magnitude, &type) != 2) {
		puts("Wrong format was used");
		return 1;
	}

	isDegrees = type == 'D';
	isRadians = type == 'R';

	if(!isDegrees && !isRadians) {
		puts("Wrong format was used");
		return 1;
	}

	if(isDegrees) {
		if(magnitude > 360 || magnitude < 0) {
			puts("Valid range of degrees is 0-360");
			return 1;
		}
		result = (PI/180)*magnitude;
	} else {
		if(magnitude > 2*PI || magnitude < 0) {
			puts("Valid range of radians is 0-2PI");
			return 1;
		}
		result = (180/PI)*magnitude;
	}

	printf("%.4f %s\n", result, isDegrees ? "radians" : "degrees");	

	return 0;
}