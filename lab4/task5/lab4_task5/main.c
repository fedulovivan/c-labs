#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int comparator(const void *a, const void *b) 
{
	int al = strlen(*((char**)a));
	int bl = strlen(*((char**)b));
	if (al > bl) {
		return 1;
	} else if (al < bl) {
		return -1;
	} else {
		return 0;
	}
}

int main()
{
	char data[256][256];
	char *rows[256];
	FILE *input_file = NULL;
	FILE *output_file = NULL;
	int i = 0;

	input_file = fopen("input.txt", "rt");
	output_file = fopen("output.txt", "wt");

	if(!input_file || !output_file) {
		puts("error opening files\n");
		return 1;
	}

	while(fgets(data[i], 256, input_file) != NULL) {
		rows[i] = data[i];
		chomp(data[i]);
		i++;
	}

	qsort(rows, i, sizeof(char *), comparator);

	while(--i >= 0) {
		fputs(rows[i], output_file);
		fputs("\n", output_file);
	}
	
	fclose(input_file);
	fclose(output_file);

	puts("all done");

	return 0;
}