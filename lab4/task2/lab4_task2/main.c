#include <stdio.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int main()
{
	char input[512];
	char* first_letters[32];
	char* chp = input;
	int word_boundary = 0;
	int i = -1;
	char* j = NULL;

	fgets(input, 512, stdin);
	chomp(input);

	// prepare array of pointers pointing to first word letters
	while(*chp) {
		if(!word_boundary && *chp != ' ') {
			word_boundary = 1;
			first_letters[++i] = chp;
		} else if (word_boundary && *chp == ' ') {
			word_boundary = 0;
		}
		chp++;
	}

	// output reversed string
	while(i >=0) {
		for(j = first_letters[i]; *j != ' ' && *j != '\0'; j++) {
			putchar(*j);
		}
		putchar(' ');
		i--;
	}
	putchar('\n');

	return 0;
}
