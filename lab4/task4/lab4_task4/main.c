#include <stdio.h>
#include <string.h>
void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
}
int main()
{
	char input[512];
	char *ch = NULL, *s = NULL, *e = NULL, *ls = NULL, *le = NULL;
	int sqe = 0, ste = 0;

	fgets(input, 512, stdin);
	chomp(input);
    ch = input;

	while(*ch) {
		if(ch == input) {
			s = e = ls = le = ch;
		} else {
			sqe = *(ch - 1) != *ch;
			ste = *(ch + 1) == '\0';
			if(!sqe || (!sqe && ste)) e = ch;
			if(sqe || ste) {	
				if(e - s > le - ls) {ls = s; le = e;}
				s = e = ch;
			} 
		}
		ch++;
	}

	while(ls <= le) {
		putchar(*ls);
		ls++;
	}	 
	putchar('\n');

	return 0;
}