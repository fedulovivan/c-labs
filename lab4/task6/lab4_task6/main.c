#include <stdio.h>
#include <limits.h>

int main()
{
	char members[100][100];
	int count = 0, int i = 0, age = 0;
	char *youngest = NULL, *oldest = NULL;
	int youngest_age = INT_MAX, oldest_age = 0;	
	
	puts("enter family size");
	if(scanf("%d", &count) != 1) {
		puts("unexpected conditions");
		return 1;
	}

	while(i < count) {

		puts("enter family member name and age");
		if(scanf("%s %d", members[i], &age) != 2) {
			puts("unexpected conditions");
		    return 1;
		}

		if(age < youngest_age) {
			youngest_age = age;
			youngest = members[i];
		}

		if(age > oldest_age) {
			oldest_age = age;
			oldest = members[i];
		}		

		i++;
	}

	printf("youngest is %s %d\n", youngest, youngest_age);
	printf("oldest is %s %d\n", oldest, oldest_age);

	return 0;
}