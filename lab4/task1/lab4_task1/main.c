#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_STRINGS 5 

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int desc_sort_by_string_length(const void *a, const void *b)
{
	int a_len = strlen(*(char**)a);
	int b_len = strlen(*(char**)b);
	if(a_len > b_len) {
		return -1;
	} else if (a_len < b_len) {
		return 1;
	} else {
		return 0;
	}
}

int main() 
{
	char strings[MAX_STRINGS][256];
	char * rows[MAX_STRINGS];
	int i = 0, j = 0;
	int empty_line = 0;

	do {
		fgets(strings[i], 256, stdin);
		empty_line = strings[i][0] == '\n';
		if(!empty_line) {
			rows[i] = strings[i];
			i++;
		}
	} while(!empty_line && i < MAX_STRINGS); 

	qsort(rows, i, sizeof(char *), desc_sort_by_string_length);

	for(j = 0; j < i; j++) {
		chomp(rows[j]);
		printf("%d. [%s]\n", j, rows[j]);
	}

	return 0;
}