#include <stdio.h>
#include <string.h>
void chomp(char *str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}
int main()
{
	char input[512];
	char *l = NULL, *r = NULL;
	fgets(input, 512, stdin);
	chomp(input);

	l = input;
	r = input + strlen(input) - 1;

	while(l < r && *l == *r) {l++; r--;}

	puts(*l == *r ? "polyndrom\n" : "not a polyndrom\n");

	return 0;
}