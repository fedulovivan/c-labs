#include <stdio.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

void update_counters(int * word_length, int * max_length, int * j, int * max_word_first_pos)
{
	if(*word_length > *max_length) {
		*max_length = *word_length;
		*max_word_first_pos = *j;
	}
}

int main()
{
	char str[256];
	int i = 0;
	int max_length = 0;
	int j = 0;
	int max_word_first_pos = 0;
	int word_boundary = 0;
	int word_length = 0;	
	int is_last_char = 0;

	fgets(str, 256, stdin);
	chomp(str);

	while(str[i]) {		
		
		if(str[i] != ' ' && !word_boundary) {
			j = i;
			word_boundary = 1;
			word_length = 0;
		} else if (str[i] == ' ' && word_boundary) {
			word_boundary = 0;
			update_counters(&word_length, &max_length, &j, &max_word_first_pos);
		} else if (i == strlen(str) - 1) {
			is_last_char = 1;
		}

		if(word_boundary) {
			word_length++;
		}

		if (is_last_char) {
			update_counters(&word_length, &max_length, &j, &max_word_first_pos);
		}

		i++;
	}

	printf("The longest word from given string is: ");
	for(i = max_word_first_pos; i < max_word_first_pos + max_length; i++) {
		putchar(str[i]);
	}
	putchar('\n');


	return 0;
}
