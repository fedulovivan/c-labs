#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_WORDS 32
#define MAX_WORD_LENGTH 128

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int main()
{

	unsigned int i;
    int j, k = -1;
	char str[256];
	char * words[MAX_WORDS];
	int word_boundary = 0;
	int tmp;
	int switched;
	int show_word_num = 0;
	int is_last_char = 0;

	fgets(str, 256, stdin);
	chomp(str);

	for(i = 0; i < strlen(str); i++) {
		tmp = word_boundary;
		if(str[i] != ' ' && !word_boundary) {
			j = -1;
			k++;
			word_boundary = 1;
			words[k] = (char*)malloc(MAX_WORD_LENGTH);
		} else if (word_boundary && str[i] == ' ') {
			word_boundary = 0;
		}
 		switched = word_boundary != tmp;
		if(i == strlen(str) - 1) {
			is_last_char = 1;
		}
		if(word_boundary) {
			j++;
			words[k][j] = str[i];
		} 
		if((switched && !word_boundary) || is_last_char) {
			j++;
			words[k][j] = '\0';
		}
	}

	puts("show n-th word:");
	if(scanf("%d", &show_word_num) != 1 || show_word_num < 1 || show_word_num > k) {
		puts("unexpected conditions");
	} else {
		puts(words[show_word_num - 1]);
	}	

	for(i = 0; i < k; i++) { free(words[i]); }

	return 0;
}