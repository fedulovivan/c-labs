#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define SIZE 10

void print_arr(const int * arr, const int size) 
{
	int i;
	for(i = 0; i < size; i++) {
		printf("%d:%d%s", i, arr[i], i < size - 1 ? ", " : "");
	}
	putchar('\n');
}

int main() 
{

	int rand_digits[SIZE];
	int negative;
	int i;
    int summ = 0;
    int last_positive_at = -1;
	int first_negative_at = -1;
	int negative_caught = 0;

	srand((unsigned int)time(0));

	for(i = 0; i < SIZE; i++) {
		negative = rand() % 2;
		rand_digits[i] = (rand() % 11) * (negative ? -1 : 1);
	}

	puts("Rand input array:");
	print_arr(rand_digits, SIZE);

	for(i = 0; i < SIZE; i++) {
		if(rand_digits[i] > 0) {
			last_positive_at = i;
		} else if (rand_digits[i] < 0) {
			if(!negative_caught) {
				first_negative_at = i;
				negative_caught = 1;
			}
		}
	}

	if(first_negative_at > -1 && last_positive_at > -1) {
		for(i = first_negative_at + 1; i < last_positive_at; i++) {
			summ += rand_digits[i];
		}
		printf("first negative at %d\n", first_negative_at);
		printf("last positive at %d\n", last_positive_at);
		puts("---");
		printf("summ of digits between first negative and last positive is %d\n", summ);
	} else {
		puts("unexpected conditions");								
	}

	return 0;
}