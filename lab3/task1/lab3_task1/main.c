#include <stdio.h>
#include <string.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int main()
{
	char str[256];
	int i = 0;
	int tmp;

	// flag indicating that currently we are inside the word
	int word_boundary = 0;
	// flag indicating that 'word_boundary' flag was switched
	int switched = 0;
	// flag indicating that we are on the last char of the string
	int is_last_char = 0;
	// temp counter for each word length
	int word_length = 0;
	// total count of words in the string
	int words_count = 0;

	fgets(str, 256, stdin);
	chomp(str);

	while(str[i]) {
		tmp = word_boundary;
		if(str[i] != ' ' && !word_boundary) {
			word_boundary = 1;
			word_length = 0;
		} else if (str[i] == ' ' && word_boundary) {
			word_boundary = 0;
			words_count++;
		}
		switched = tmp != word_boundary;
		if (i == strlen(str) - 1) {
			is_last_char = 1;
		}

		// if we are inside the word output its current char and increase length counter
		if(word_boundary) {
			word_length++;
			putchar(str[i]);
		}

		// show length if the word is just ended or we already reached end of the string not leaving word boundary
		if((!word_boundary && switched) || (word_boundary && is_last_char)) {
			printf(" %d\n", word_length);
		} 
		i++;
	}

	printf("%d words counted\n", words_count);

	return 0;
}
