#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void chomp(char * str)
{
	if(str[strlen(str) - 1] == '\n') {
		str[strlen(str) - 1] = 0;
	}
}

int is_digit(char ch)
{
	return ch >= '0' && ch <= '9';
}

void process_ready_digit(char * digit, const int * j, int * summ)
{
	int real_digit;
	digit[*j] = '\0';
	puts(digit);
	real_digit = atoi(digit);
	*summ += real_digit;
}

int main()
{
	const int max_len = 5;
	char str[256];
	int i = 0, j = 0;
	int digit_boundary = 0;
	int is_last_char = 0;
	char digit[128];
	int summ = 0;

	fgets(str, 256, stdin);
	chomp(str);

	while(str[i]) {		

		if(is_digit(str[i]) && !digit_boundary) {
			j = 0;
			digit_boundary = 1;
		} else if (!is_digit(str[i]) && digit_boundary) {
			digit_boundary = 0;
			process_ready_digit(digit, &j, &summ);
		}
		
		if (i == strlen(str) - 1) {
			is_last_char = 1;
		}

		if(digit_boundary) {
			digit[j] = str[i];
			j++;
		}

		if(digit_boundary && is_last_char) {
			process_ready_digit(digit, &j, &summ);
		}

		i++;
	}

	printf("summ is %d\n", summ);

	return 0;
}
