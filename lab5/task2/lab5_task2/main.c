#include <stdio.h>
#include <time.h>
#include <stdlib.h>

#define FIELD_SIZE 20

typedef unsigned int coord;
typedef unsigned int asize;

void sleep(int delay)
{
	clock_t now = clock(); while(clock() < now + delay);
}

void truncate_field(char (*field)[FIELD_SIZE])
{
	int i, j;
	for(i = 0; i < FIELD_SIZE; i++)	{
		for(j = 0; j < FIELD_SIZE; j++) {
			field[i][j] = 0;
		}
	}
}

void print_field(char (*field)[FIELD_SIZE])
{
	int i, j;
	for(i = 0; i < FIELD_SIZE; i++)	{
		for(j = 0; j < FIELD_SIZE; j++) {
			putchar(field[i][j]);
		}
	    putchar('\n');
	}
	putchar('\n');
}

/**
 * Populate first quadrant with random data
 */
void populate_randomly_top_left_quadrant(char (*field)[FIELD_SIZE])
{
	int count = (FIELD_SIZE * FIELD_SIZE) / (5 * 2 * 2);	
	coord rand_x, rand_y;
	srand((unsigned int)time(0));
	while(count) {
		rand_x = rand() % (FIELD_SIZE / 2);
		rand_y = rand() % (FIELD_SIZE / 2);
		if(field[rand_x][rand_y] != '*') {
			field[rand_x][rand_y] = '*';
			count--;
		}
	}
}

/**
 * 	Copy source area identified by x,y coordinates of top left corner and x,y coordinates of bottom right corner
 *  to destination area identified by x,y coordinates of its top left corner (bottom right corner is calculated automatically)
 *  optionally copied data could be mirrored by either x or y axes
 */
int copy_mirrored(char (*field)[FIELD_SIZE], coord src_tl_x, coord src_tl_y, coord src_br_x, coord src_br_y, coord dst_tl_x, coord dst_tl_y, int mirror_by_x, int mirror_by_y)
{
	unsigned int i, j, dst_x, dst_y;

	asize width  = (src_br_x - src_tl_x) + 1;
	asize height = (src_br_y - src_tl_y) + 1;

	if(width == 0 || height == 0) return 1;
	if(src_tl_x > FIELD_SIZE - 1 || src_tl_y > FIELD_SIZE - 1 || src_br_x > FIELD_SIZE - 1 || src_br_y > FIELD_SIZE - 1) return 2;
	if(dst_tl_x > FIELD_SIZE - 1 || dst_tl_y > FIELD_SIZE - 1) return 3;
	if(dst_tl_x + width > FIELD_SIZE || dst_tl_y + height > FIELD_SIZE) return 4;

	for(i = src_tl_x; i <= src_br_x; i++) {
		for(j = src_tl_y; j <= src_br_y; j++) {
			dst_x = (mirror_by_y ? (width - i - 1) : i) + dst_tl_x;
			dst_y = (mirror_by_x ? (height - j - 1) : j) + dst_tl_y;
			field[dst_x][dst_y] = field[i][j];
		}
	}
	
	return 0;
}

/**
 *	Copy randomly generated data to the rest three quadrants
 */
void propagate_to_the_rest_of_quadrants(char (*field)[FIELD_SIZE]) 
{
	int res;

	res = copy_mirrored(field, 0, 0, FIELD_SIZE / 2 - 1, FIELD_SIZE / 2 - 1, FIELD_SIZE / 2, 0, 0, 1);
	if(res != 0) printf("copying failed with error %d/n", res);

	res = copy_mirrored(field, 0, 0, FIELD_SIZE / 2 - 1, FIELD_SIZE / 2 - 1, 0, FIELD_SIZE / 2, 1, 0);
	if(res != 0) printf("copying failed with error %d/n", res);

	res = copy_mirrored(field, 0, 0, FIELD_SIZE / 2 - 1, FIELD_SIZE / 2 - 1, FIELD_SIZE / 2, FIELD_SIZE / 2, 1, 1);
	if(res != 0) printf("copying failed with error %d/n", res);
}

int main()
{
	char field[FIELD_SIZE][FIELD_SIZE] = {0};
	while(1) {
		truncate_field(field);
		populate_randomly_top_left_quadrant(field);
		propagate_to_the_rest_of_quadrants(field);
		print_field(field);
		sleep(500);
	}
	return 0;
}