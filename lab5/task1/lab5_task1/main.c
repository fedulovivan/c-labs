#include <stdio.h>
#include <string.h>
#include <time.h>
#include <stdlib.h>

#define SIZE 512

void chomp(char *str)
{
	if(str[strlen(str) - 1] == '\n') str[strlen(str) - 1] = '\0';
}

int rnd_comparator(const void *a, const void *b)
{
	int rand_number, negative; 
	srand((unsigned int)time(0));		
	rand_number = rand() % 2;
	negative = rand() % 2;
	return rand_number * (negative ? -1 : 1);
}

void randomize_words(char *words[SIZE], int count)
{
	qsort(words, count, sizeof(char*), rnd_comparator);
}

void print_word(char *word, char delimiter)
{
	while(*word != ' ' && *word != '\0') {
		putchar(*word);
		word++;
	}
	putchar(delimiter);
}

int parse_words(const char *input, char *words[SIZE])
{
	int word_boundary = 0;
	char *p = (char*)input;
	int i = 0;
	while(*p) {
		if(!word_boundary && *p != ' ') {
			word_boundary = 1;
			words[i++] = p;
		} else if (word_boundary && *p == ' ' || *(p + 1) == '\0') {
			word_boundary = 0;
		}
		p++;
	}
	return i;
}

int main() 
{
	char input[SIZE];
	char *words[SIZE] = {NULL};
	int i, count;

	fgets(input, SIZE, stdin);
	chomp(input);

	count = parse_words((const char*)input, words);

	randomize_words(words, count);

	for(i = 0; i < count; i++) {
		print_word(words[i], ' ');
	}
	putchar('\n');

	return 0;
}