#include <stdio.h>
#include <stdlib.h>
#include <string.h>

//#define INPUT_FILE "var_and_peace.txt"
//#define INPUT_FILE "input.txt"
//#define INPUT_FILE "01.png"
#define INPUT_FILE "full.txt"

#define OUTPUT_FILE "result"

#define MAX_ALPHABET_LENGTH 512

// representation of single alphabet character
typedef struct SYM
{
	unsigned char ch;
	int count;
	float freq;
	char code[256];
	struct SYM *left;
	struct SYM *right;
};

// auxiliary union for bit-wise operations on single char
typedef union CODE
{
	unsigned char ch;
	struct BYTE
	{
		unsigned b1:1;
		unsigned b2:1;
		unsigned b3:1;
		unsigned b4:1;
		unsigned b5:1;
		unsigned b6:1;
		unsigned b7:1;
		unsigned b8:1;
	} byte;
};

// --------------------------------------
// Reading file contents
// --------------------------------------
// file_name	- name of input file which is located in the same directory with binary
// input		- pointer to array where all data be kept
// chars_count  - pointer to variable which be populated with count of chars read from file
// --------------------------------------
int read_file(const char* file_name, unsigned char** input, unsigned long* chars_count)
{
	FILE* fp = fopen(file_name, "rb");
	if(!fp) {return -1;}
	fseek(fp, 0L, SEEK_END);
	*chars_count = (unsigned long)ftell(fp);
	fseek(fp, 0L, SEEK_SET);
	if(*chars_count == 0) {return -2;}
    *input = (unsigned char*)malloc(sizeof(char)*(*chars_count));
	if(!*input) {return -3;}
	fread(*input, sizeof(char), *chars_count, fp);
	fclose(fp);
	return 1;
}

// --------------------------------------
// Collect statistics on chars from file
// --------------------------------------
// input		- pointer to array with chars from file
// syms			- pointer to array of structures with chars
// syms_count   - pointer to variable which will be populated with count of unique chars in file
// chars_count	- count of chars in file
// --------------------------------------
int collect_stat(unsigned char** input, struct SYM** syms, unsigned long *syms_count, const unsigned long chars_count)
{
	int i, j, k, exist;
	*syms = (struct SYM*)malloc(sizeof(struct SYM)*MAX_ALPHABET_LENGTH);
	if(!*syms) {return -1;}
	for(j = 0; j < MAX_ALPHABET_LENGTH; j++) {
		((*syms)[j]).ch = 0;
		((*syms)[j]).count = 0;
		((*syms)[j]).freq = 0L;
		((*syms)[j]).code[0] = 0;
		((*syms)[j]).left = ((*syms)[j])	.right = NULL;
	}
	k = 0;
	for(i = 0; i < chars_count; i++) {
		exist = 0;
		for(j = 0; j < MAX_ALPHABET_LENGTH; j++) {
			if(((*syms)[j]).ch == (*input)[i]) {
				((*syms)[j]).count++;
				exist = 1;
				break;
			}
			if(j + 1 < MAX_ALPHABET_LENGTH && ((*syms)[j+1]).count == 0) { break; }
		}
		if(!exist) {
		   ((*syms)[k]).ch = (*input)[i]; 
		   ((*syms)[k]).count = 1;
		   k++;
		}
	}
	for(j = 0; j < MAX_ALPHABET_LENGTH; j++) {
		((*syms)[j]).freq = (float)(((*syms)[j]).count) / chars_count;
		if(j + 1 < MAX_ALPHABET_LENGTH && ((*syms)[j+1]).count == 0) { 
			*syms_count = j + 1;
			break; 
		}
    }
	return 1;
}

// --------------------------------------
// Printing array of syms, used only for debugging
// --------------------------------------
// syms			- array of structures with characters
// syms_count   - count of unique chars fount in file
// chars_count  - total count of chars read from file
// --------------------------------------
void print_syms(struct SYM **syms, const unsigned long syms_count, const unsigned long chars_count) 
{
	int j;
	printf("File contains %d chars with following distribution:\n", chars_count);
	for(j = 0; j < syms_count; j++) {
		printf(
			"%d) ch=%d, count=%d, frequency=%.5f, code=%s\n",
			j + 1,
			((*syms)[j]).ch,
			((*syms)[j]).count,
			((*syms)[j]).freq,
			((*syms)[j]).code
		);
	}
}

int sym_freq_desc_comparator(const void *a, const void *b) 
{
	float a_freq = (*((struct SYM**)a))->freq;
	float b_freq = (*((struct SYM**)b))->freq;
	if (a_freq < b_freq) {
		return 1;
	} else if (a_freq > b_freq) {
		return -1;
	} else {
		return 0;
	}
}

// --------------------------------------
// Descending sort by char frequency
// --------------------------------------
void sort_psyms_desc(struct SYM *psyms[], const long alphabet_length)
{
	qsort(psyms, alphabet_length, sizeof(struct SYM *), sym_freq_desc_comparator);
}

// --------------------------------------
// Building Huffman tree
// --------------------------------------
struct SYM * build_tree(struct SYM *psyms[], const long alphabet_length)
{
	struct SYM *temp_sym;

	sort_psyms_desc(psyms, alphabet_length);

	temp_sym = (struct SYM*)malloc(sizeof(struct SYM));
	temp_sym->freq = psyms[alphabet_length - 1]->freq + psyms[alphabet_length - 2]->freq;
	temp_sym->left = psyms[alphabet_length - 1];
	temp_sym->right = psyms[alphabet_length - 2];
	temp_sym->code[0] = 0;
	temp_sym->count;
	temp_sym->ch = 0;

	psyms[alphabet_length - 2] = temp_sym;
	psyms[alphabet_length - 1] = NULL;

	if(alphabet_length == 2) return temp_sym;

	return build_tree(psyms, alphabet_length - 1);
}

// --------------------------------------
// Creating prefix code record for each alphabet letter
// --------------------------------------
void populate_codes(struct SYM *root)
{
    if(root->left) {
		strcpy(root->left->code, root->code);
		strcat(root->left->code, "0");
		populate_codes(root->left);
	}
	if(root->right) {
		strcpy(root->right->code, root->code);
		strcat(root->right->code, "1");
		populate_codes(root->right);
	}
}

// --------------------------------------
// Retrieve prefix code by value of char
// --------------------------------------
char * get_code(struct SYM syms[], const unsigned long syms_count, unsigned char ch)
{
	int j;
	for(j = 0; j < syms_count; j++) {
		if(syms[j].ch == ch) return syms[j].code;
	}
	return "";
}

// --------------------------------------
// Pack source data into array of chars
// --------------------------------------
// input
// syms
// alphabet_length
// chars_count
// packed_chunks_count
// tail_size
// --------------------------------------
unsigned char * pack_input(const unsigned char *input, struct SYM *syms, 
	const unsigned long alphabet_length, const unsigned long chars_count,
	int * packed_chunks_count, int * tail_size
) {

	int i, read_bytes;
	char * code, * buf;
	unsigned char * packed; 
	unsigned long tmp_file_size;
	union CODE packer;
	FILE * tmp_file;

	tmp_file = fopen("tmp", "w+b");

	for(i = 0; i < chars_count; i++) {
		code = get_code(syms, alphabet_length, input[i]);
		fwrite(code, strlen(code), 1, tmp_file);
	}

	fseek(tmp_file, 0L, SEEK_END);
	tmp_file_size = (unsigned long)ftell(tmp_file);
	fseek(tmp_file, 0L, SEEK_SET);

	*tail_size = tmp_file_size % 8;
	*packed_chunks_count = tmp_file_size / 8 + (tail_size > 0 ? 1 : 0);	

	buf = (char*)malloc(sizeof(char)*8);
	packed = (unsigned char*)malloc(sizeof(unsigned char) * (*packed_chunks_count));

	i = 0;
	while(read_bytes = fread(buf, sizeof(char), 8, tmp_file)) {
		packer.byte.b8 = buf[0] == '1';
		packer.byte.b7 = read_bytes > 1 ? buf[1] == '1' : 0;
		packer.byte.b6 = read_bytes > 2 ? buf[2] == '1' : 0;
		packer.byte.b5 = read_bytes > 3 ? buf[3] == '1' : 0;
		packer.byte.b4 = read_bytes > 4 ? buf[4] == '1' : 0;
		packer.byte.b3 = read_bytes > 5 ? buf[5] == '1' : 0;
		packer.byte.b2 = read_bytes > 6 ? buf[6] == '1' : 0;
		packer.byte.b1 = read_bytes > 7 ? buf[7] == '1' : 0;
		packed[i++] =  packer.ch;
	}

	free(buf);
	fclose(tmp_file);
	//remove("tmp");
	return packed;
}

// --------------------------------------
// Write result into output file
// --------------------------------------
// syms
// alphabet_length
// tail_size
// packed_chunks_count
// packed
// --------------------------------------
void write_file(struct SYM *syms, const unsigned long alphabet_length, const int tail_size, const int packed_chunks_count,  unsigned char * packed)
{
	int i;
	FILE * out_file;

	out_file = fopen(OUTPUT_FILE, "wb");

	// alphabet length (1 byte)
	fwrite(&alphabet_length, 1, sizeof(unsigned char), out_file);

	// alphabet itself (1 byte + 4 bytes) * alphabet length
	for(i = 0; i < alphabet_length; i++) {
		fwrite(&syms[i].ch, 1, sizeof(unsigned char), out_file);
		fwrite(&syms[i].freq, 1, sizeof(float), out_file);
	}

	// tail size (1 byte)
	fwrite(&tail_size, 1, sizeof(unsigned char), out_file);

	// write packed data itself
	fwrite(packed, packed_chunks_count, sizeof(unsigned char), out_file);

	fclose(out_file);

}

int main()
{

	int i, res, packed_chunks_count, tail_size;
	struct SYM * syms, * psyms[MAX_ALPHABET_LENGTH], * root;
	unsigned char * input, * packed;
	unsigned long chars_count, alphabet_length;	
	
	printf("Compressing...\n");

	// read input file, get chars count
	if(read_file(INPUT_FILE, &input, &chars_count) != 1) {
		printf("failed to read input file with err code %d", res);
		exit(0);
	}
						
	// retrieve character occurrence statistics, get alphabet length
	if(collect_stat(&input, &syms, &alphabet_length, chars_count) != 1) {
		printf("failed to collect stat with err code %d", res);
		exit(0);
	}

	// create auxiliary array populated with pointers to alphabet
	for(i = 0; i < MAX_ALPHABET_LENGTH; i++) { psyms[i] = &(syms[i]); }

	// build Huffman tree from given alphabet
	root = build_tree(psyms, alphabet_length);

	// create prefix code record for each alphabet letter
	populate_codes(root);

	// pack source data into array of chars using created codes
	packed = pack_input(input, syms, alphabet_length, chars_count, &packed_chunks_count, &tail_size);

	// write result into output file
	write_file(syms, alphabet_length, tail_size, packed_chunks_count, packed);

	printf("Done. Compression ratio: %.2f\n", chars_count/(float)packed_chunks_count);

	return 0;

}