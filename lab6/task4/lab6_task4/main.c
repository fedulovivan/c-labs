#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#define M 10
#define N 25

#define CURSOR 'A'

#define SLEEP 100

void sleep(int delay)
{
	clock_t now = clock(); while(clock() < now + delay);
}

int read_area(char *file_name, char(*data)[N])
{
	int i = 0, j = 0;
	char ch;
	FILE *file = fopen(file_name, "rt");
	if(!file) return 1;
	while (i < M && j < N) {
	    ch = fgetc(file);
		if(ch == '\n' || ch == EOF) continue;
		data[i][j] = ch;
		if(j < N - 1) {
			j++;
		} else {
			i++;
			j = 0;
		}
	}
    fclose(file);
	return 0;
}

void print_area(char(*area)[N])
{
	int i, j;
	system("cls");
	for(i = 0; i < M; i++) {
		for(j = 0; j < N; j++) {
			putc(area[i][j], stdout);
		}
		putc('\n', stdout);
	}
	putc('\n', stdout);
}

int exit_found(const int i, const int j)
{
	return i == 0 || i == M - 1 || j == 0 || j == N - 1;
}

int can_move_to(char(*area)[N], const int i, const int j)
{
    return i >=0 && i < M && j >= 0 && j < N && area[i][j] == ' ';
}

int go(char(*area)[N], const int i, const int j, char direction) 
{
	int coord_left[2]			= {i, j};
	int coord_right[2]			= {i, j};
	int coord_sraight_ahead[2]  = {i, j};
	int coord_uturn[2]          = {i, j};
	char new_left_direction;
	char new_right_direction;
	char new_uturn_direction;
	
	if(i >= M || j >= N) {
		puts("position is outside of the area");
		return -1;
	}
	if(area[i][j] != ' ') {
		puts("invalid position on the area");
		return -2;						
	}

	sleep(SLEEP);
	area[i][j] = CURSOR;
	print_area(area);

	switch(direction) {
		case 'n':
			new_left_direction = 'w';
			new_right_direction = 'e';
			new_uturn_direction = 's';
			coord_left[1] = j - 1;
			coord_right[1] = j + 1;
			coord_sraight_ahead[0] = i - 1;
			coord_uturn[0] = i + 1;
			break;
		case 'e':
			new_left_direction = 'n';
			new_right_direction = 's';
			new_uturn_direction = 'w';	 
			coord_left[0] = i - 1;
			coord_right[0] = i + 1;
			coord_sraight_ahead[1] = j + 1;
			coord_uturn[1] = j - 1;
			break;
		case 's':
			new_left_direction = 'e';
			new_right_direction = 'w';
			new_uturn_direction = 'n';
			coord_left[1] = j + 1;
			coord_right[1] = j - 1;
			coord_sraight_ahead[0] = i + 1;
			coord_uturn[0] = i - 1;
			break;
		case 'w':
			new_left_direction = 's';
			new_right_direction = 'n';
			new_uturn_direction = 'e';
			coord_left[0] = i + 1;
			coord_right[0] = i - 1;
			coord_sraight_ahead[1] = j - 1;
			coord_uturn[1] = j + 1;
			break;
	}

	if(exit_found(i, j)) {
		return 0;
	} else if (can_move_to(area, coord_left[0], coord_left[1])) {
		area[i][j] = ' ';
		return go(area, coord_left[0], coord_left[1], new_left_direction);
	} else if (can_move_to(area, coord_sraight_ahead[0], coord_sraight_ahead[1])) {
		area[i][j] = ' ';
		return go(area, coord_sraight_ahead[0], coord_sraight_ahead[1], direction);
	} else if (can_move_to(area, coord_right[0], coord_right[1])) {	
		area[i][j] = ' ';
		return go(area, coord_right[0], coord_right[1], new_right_direction);
	} else if(can_move_to(area, coord_uturn[0], coord_uturn[1])) {
		area[i][j] = ' ';
		return go(area, coord_uturn[0], coord_uturn[1], new_uturn_direction);
	} else {
		return -3;	
		puts("unexpected conditions");
	}

}

int main()
{
	char area[M][N];
	int res;

	if(read_area("labyrinth10x25.txt", area) != 0) {
		puts("failed to read input file");
		return 1;
	}

	res = go(area, 3, 10, 'n');

	if(res != 0) {
		printf("failed with error code %d\n", res);
		return 1;	
	}
	puts("exit was found");
	return 0;
}