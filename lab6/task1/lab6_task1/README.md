*Lab6, Task1*
Program calculates sum of array using two approaches: classical(inside for loop) and recursive.

Program should ran following steps:
1. Accept a number from command line
2. Calculates size of dynamic array as N = 2 ^ M, where M is number from step 1
3. allocate memory for array
4. populate array with random data
5. find sum of array using two mentioned approaches
6. compare performance of two approaches
7. frees dynamically allocated memory