#define _CRT_SECURE_NO_WARNINGS

#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <time.h>
#include <limits.h>
#include <errno.h>

int summ(long *arr, int m, int n)
{
	int middle;
	if(n - m == 1) {
		return arr[n] + arr[m];
	} else {
		middle = (n - m)/2 + m;
		return summ(arr, m,  middle) + summ(arr, middle + 1, n);
	}
};

int main()
{
	float power = 0;
	long* numbers_array;
	int array_size;
	int i;
	int rand_value;
	long result;
	clock_t before;
	
	srand((unsigned int)time(0));

	puts("give power of 2 (5-25):");
	if(scanf("%f", &power) != 1 || power < 5 || power > 26) {
		puts("invalid value given");
		return 1;
	}

	array_size = (int)pow(2, power);

	numbers_array = (long*)malloc(array_size * sizeof(long));

	if(!numbers_array) {
		puts("failed to allocate memory");
		return 1;
	}

	for(i = 0; i < array_size; i++) {
		rand_value = rand() % 100 * (rand() % 2 ? -1 : 1);
		numbers_array[i] = rand_value;
	}

	printf("array of %d elements allocated and populated with random data\n", array_size);

	puts("calculating array sum now...");

	before = clock();
	result = 0;
	for(i = 0; i < array_size; i++) {result += numbers_array[i];}
	printf("for loop: %d done in %d ms\n", result, clock() - before);

	before = clock();
	result = summ(numbers_array, 0, array_size - 1);
	printf("recursion: %d done in %d ms\n", result, clock() - before);

	free(numbers_array);

	return 0;
}